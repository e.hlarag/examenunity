using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class EnemigoSO : ScriptableObject
{
    // Start is called before the first frame update
    public Color color;
    public float spd;
    public int dmg;
}
