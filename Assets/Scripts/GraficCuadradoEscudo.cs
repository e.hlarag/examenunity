using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GraficCuadradoEscudo : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject escudo;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
        if (escudo.GetComponent<Script1>().hp<=7 && escudo.GetComponent<Script1>().hp > 5)
        {
            this.GetComponent<Image>().color = Color.green;
        }else if (escudo.GetComponent<Script1>().hp <= 5 && escudo.GetComponent<Script1>().hp > 2)
        {
            this.GetComponent<Image>().color = Color.yellow;
        }else if (escudo.GetComponent<Script1>().hp > 0 && escudo.GetComponent<Script1>().hp <= 2)
        {
            this.GetComponent<Image>().color = Color.red;
        }
    }
}
