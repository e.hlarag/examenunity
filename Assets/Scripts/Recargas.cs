using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Recargas : MonoBehaviour
{
    public GameObject operario;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(carga());
    }

    // Update is called once per frame
    void Update()
    {

    }

    public IEnumerator carga()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.5f);
            GameObject funcionario = Instantiate(operario);
            funcionario.transform.position = new Vector3(-9.92f, 0, 0);
            Debug.Log(funcionario.GetComponent<Rigidbody2D>().velocity);
            int r = Random.Range(1, 3);
            if (r == 1)
            {
                funcionario.GetComponent<SpriteRenderer>().color = Color.red;
            }else if (r == 2)
            {
                funcionario.GetComponent<SpriteRenderer>().color = Color.green;
            }
            funcionario.GetComponent<Rigidbody2D>().AddForce(transform.right * 100);
        }
    }
}
