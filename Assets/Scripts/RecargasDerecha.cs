using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RecargasDerecha : MonoBehaviour
{
    // Start is called before the first frame update
    public delegate void cuadrados();
    public cuadrados oncuadrado;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnMouseDown()
    {
        if (this.GetComponent<SpriteRenderer>().color==Color.red) 
        {
            SceneManager.LoadSceneAsync(0);
        }
        else if(this.GetComponent<SpriteRenderer>().color == Color.green)
        {
            oncuadrado.Invoke();
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "derecha" && this.GetComponent<SpriteRenderer>().color==Color.green)
        {
            SceneManager.LoadSceneAsync(0);
        }

    }
}
