using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;

public class Script1 : MonoBehaviour
{
    // Start is called before the first frame update
    public delegate void vida();
    public vida onvida;
    public int hp = 7;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.D))
        {
            this.transform.parent.Rotate(0f, 0f, -1);
        }
        if (Input.GetKey(KeyCode.A))
        {
            this.transform.parent.Rotate(0f, 0f, 1);
        }

        if (this.hp <= 0)
        {
            this.gameObject.SetActive(false);
            SceneManager.LoadSceneAsync(1);
        }

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "enemigo")
        {
            collision.gameObject.SetActive(false);
            onvida.Invoke();
            this.hp--;

        }
    }
}
