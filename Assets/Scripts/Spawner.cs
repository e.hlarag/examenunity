using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject enemigos;
    public float spd;
    public GameObject estrella;
    void Start()
    {
        StartCoroutine(Spawnear());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public IEnumerator Spawnear()
    {
        while (true)
        {
            yield return new WaitForSeconds(2f);
            for (int i = 0; i < this.transform.childCount; i++)
            {
                if (!this.transform.GetChild(i).gameObject.active)
                {

                    List<Vector2> posiciones = new List<Vector2>();
                    Vector2 posicionrandom = new Vector2(Random.Range(-10, 10), Random.Range(-5, 5));
                    this.transform.GetChild(i).gameObject.SetActive(true);


                    this.transform.GetChild(i).transform.position = posicionrandom;
                    Vector2 cero = new Vector2(0, 0);
                    //int r = Random.Range(0, posiciones.Count);
                    Vector3 distancia = this.transform.GetChild(i).transform.position - estrella.transform.position;
                    this.transform.GetChild(i).transform.up = distancia;
                    this.transform.GetChild(i).GetComponent<Rigidbody2D>().velocity = -this.transform.GetChild(i).transform.up * spd;
                    break;
                }
            }
            
        }

    }
}
