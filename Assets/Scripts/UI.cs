using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UI : MonoBehaviour
{
    public GameObject texto;
    public GameObject textohp;
    public GameObject boton;
    public GameObject escudo;
    int cuentaatras = 60;
    int hp = 7;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(contador());
        this.escudo.GetComponent<Script1>().onvida += restarvida;
        
    }

    // Update is called once per frame
    void Update()
    {
        if (this.cuentaatras == 0)
        {
            this.boton.SetActive(true);
            print("Win");
        }
        this.textohp.GetComponent<TextMeshProUGUI>().text = "HP: " + this.hp;

        
    }

    public IEnumerator contador()
    {
        while (cuentaatras > 0)
        {
            yield return new WaitForSeconds(1f);
            this.cuentaatras--;
            this.texto.GetComponent<TextMeshProUGUI>().text = "" + cuentaatras + "s";
        }
    }

    public void restarvida()
    {
        this.hp--;
    }
}
